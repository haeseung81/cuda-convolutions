#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <cv.h>
#include <highgui.h>

extern "C" cudaError_t addWithCuda(int *c, const int *a, const int *b, size_t size);
extern "C" __global__ void addKernel(int *c, const int *a, const int *b);
extern "C" void gpu_Filter(float *pcuSrc,float *pcuDst, int w, int h,float *cuGkernel,int kernel_size);

void Seq_Gaborfilter(float Gvar, float Gtheta, float Glambda, float Gpsi, int Gkernel_size,float *Gkernel)
{
	if (Gkernel_size%2==0)
		Gkernel_size++;

	for (int x = -Gkernel_size/2;x<=Gkernel_size/2; x++) {
		for (int y = -Gkernel_size/2;y<=Gkernel_size/2; y++) {
			int index = (x+Gkernel_size/2)*Gkernel_size + (y+Gkernel_size/2);
			Gkernel[index] =  exp(-((x*x)+(y*y))/(2*Gvar))*cos(Glambda*(x*cos(Gtheta)+y*sin(Gtheta))+Gpsi);
		} 
	}
}

void Seq_Gaussianfilter(float sigma, int Gkernel_size,float *Gkernel)
{
	if (Gkernel_size%2==0)
		Gkernel_size++;
	
	float r, s = 2.0*sigma*sigma;
	float pi = 3.14159265;
	float sum = 0;
	
	for (int x = -Gkernel_size/2;x<=Gkernel_size/2; x++) {
		for (int y = -Gkernel_size/2;y<=Gkernel_size/2; y++) {
			r = sqrt(float(x*x + y*y));
			int index = (x+Gkernel_size/2)*Gkernel_size + (y+Gkernel_size/2);			
			Gkernel[index] = (exp(-(r*r)/s)/(pi*s));
			sum += Gkernel[index];
		} 
	}
	
	for (int i=0; i<Gkernel_size*Gkernel_size;i++)
		Gkernel[i] /= sum;	
}

void seq_Filter(float* kernel, int k_size,float* src_img, float* dst_img, int h, int w)
{
	int i=0;
	int j=0;
				
	for(int i=0; i<h; i++)
	{
		for(int j=0; j<w; j++)
		{
			int index = i*w+j;
			if (j>k_size/2&&i>k_size/2 && j<w-k_size/2 && i<h-k_size/2)
			{					
				float temp = 0;
				int border = 0;
				for (int y=0; y<k_size; y++)
				{
					for (int x=0; x<k_size; x++)
					{
						border = (y-k_size/2+i)*w+(x-k_size/2+j);
						temp += (src_img[border]*kernel[y*k_size+x]);						
					}
				}
			dst_img[index] = temp;
			}
			else
			{
				dst_img[index] =0;
			}
		}		
	}
}

int main()
{
	//
	IplImage* pInput = cvLoadImage("../input/resize_baboon.jpg",CV_LOAD_IMAGE_GRAYSCALE); //load image using opencv
	// input image display ----------------------------
	cvNamedWindow("input image1",CV_WINDOW_AUTOSIZE);
	cvShowImage("input image1",pInput);
	// ------------------------------------------------
	//cvWaitKey(0);
	int w = pInput->width ;

	int ws = pInput->widthStep;
	int h = pInput->height ;

	printf("%d\t%d\t%d\n",h,w,ws);

	float *pSrc = new float[w*h];
	float *pDst_gpu = new float[w*h];
	float *pDst_seq = new float[w*h];

	for (int j = 0; j < h; j++) {
		for (int i = 0; i < w; i++)
		{ 
			pSrc[j*w+i] = (unsigned char)(pInput->imageData[j*ws+i]) ;
		}	 
	}
		
	float *pcuSrc;
	float *pcuDst;
	float *pcuGkernel;
	
	// kernel size
	int kernel_size=31;
		
	float *Gkernel = new float[kernel_size*kernel_size];
	// Gabor filter	for texture
	//Seq_Gaborfilter(1.5,(90.0*3.141593/180),(0.55),(90.0*3.141593/180),kernel_size,Gkernel);
	// Gaussian filter for smoothing
	Seq_Gaussianfilter(3.0,kernel_size,Gkernel);
	
	// gpu
	int64 tStart = cvGetTickCount();
	// Allocate cuda device memory
	(cudaMalloc((void**) &pcuSrc, w*h*sizeof(float)));
	(cudaMalloc((void**) &pcuDst, w*h*sizeof(float)));	
		
	// copy input image across to the device	
	(cudaMemcpy(pcuSrc,  pSrc, w*h*sizeof(float), cudaMemcpyHostToDevice));
		
	(cudaMalloc((void**) &pcuGkernel, kernel_size*kernel_size*sizeof(float)));
	(cudaMemcpy(pcuGkernel, Gkernel, kernel_size*kernel_size*sizeof(float), cudaMemcpyHostToDevice));
		
	gpu_Filter(pcuSrc,pcuDst, w, h,pcuGkernel,kernel_size);
	
	// Copy the marker data back to the host
	(cudaMemcpy(pDst_gpu, pcuDst, w*h*sizeof(float), cudaMemcpyDeviceToHost));
	
	int64 tEnd = cvGetTickCount();// for check processing time
	double time = 0.001 * (tEnd - tStart) / cvGetTickFrequency();
	printf("GPU Processing time : %f msec\n", time);

	// visualization ------------------------------------------
	CvSize cvsize1 = {w ,h};
	//IplImage* TempImage1 = cvCreateImage( cvsize1, IPL_DEPTH_32F, 1); 
	IplImage* gpu_rst = cvCreateImage( cvsize1, IPL_DEPTH_8U, 1); 
 
	//	IplImage* dst = cvCreateImage( cvsize1, IPL_DEPTH_8U, 1); 

	
	// copy to OpenCV 
	for (int y = 0; y < cvsize1.height; y++) {
		for (int x = 0; x < cvsize1.width; x++) { 
			cvSetReal2D(gpu_rst, y, x, pDst_gpu[y*cvsize1.width+x]);
		}
	}

	cvSaveImage("result_gpu.bmp",gpu_rst);
	cvNamedWindow("sGFilteredGPU",CV_WINDOW_AUTOSIZE);
	cvShowImage("sGFilteredGPU", gpu_rst );

	cvWaitKey(0);
	// ------------------------------------------

	// cpu ------------------------------------------------
	tStart = cvGetTickCount();
	seq_Filter(Gkernel, kernel_size, pSrc, pDst_seq, h, w);
	tEnd = cvGetTickCount();// for check processing time
	time = 0.001 * (tEnd - tStart) / cvGetTickFrequency();
	printf("Sequential processing time : %f msec\n", time);

	IplImage* seq_rst = cvCreateImage( cvsize1, IPL_DEPTH_8U, 1);

	for (int y = 0; y < cvsize1.height; y++) {
		for (int x = 0; x < cvsize1.width; x++) { 
			cvSetReal2D(seq_rst, y, x, pDst_seq[y*cvsize1.width+x]);
		}
	}

	cvSaveImage("result_seq.bmp",seq_rst);
	cvNamedWindow("sGFilteredSeq",CV_WINDOW_AUTOSIZE);
	cvShowImage("sGFilteredSeq", seq_rst );

	cvWaitKey(0);
	// -------------------------------------------------------------
	// free the device memory

	cudaFree(pcuSrc);
	cudaFree(pcuDst);
	cvReleaseImage(&gpu_rst);
	cvReleaseImage(&seq_rst);
	return 0;
}
