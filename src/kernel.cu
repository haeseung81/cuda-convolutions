
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>

 extern "C" void gpu_Filter(float *pcuSrc,float *pcuDst, int w, int h, float *cuGkernel, int kernel_size);
 


__global__ void cuda_Filter2D(float * pSrcImage, int SrcWidth, int SrcHeight,float *pKernel, int KWidth, int KHeight,float *pDstImage)
{
	//int tx = threadIdx.x;
	//int ty = threadIdx.y;
	/*extern __shared__ float gmat[];
	
	if (tx<KWidth && ty<KHeight)
	{
		gmat[ty*KWidth+tx] = pKernel[ty*KWidth+tx];
	}*/
	__syncthreads();

	int x = blockIdx.x*blockDim.x + threadIdx.x;
	int y = blockIdx.y*blockDim.y + threadIdx.y;
	int index = y*SrcWidth+x;
	int border;
	float temp;
		
	//	input[index] = clamp1(input);
	if (x>KWidth/2&&y>KHeight/2 &&x < SrcWidth-KWidth/2 && y < SrcHeight-KHeight/2)
	{		
		temp = 0;
		for (int i=0; i<KHeight; i++)
			{
				for (int j=0; j<KWidth; j++)
				{
					border = (y-KHeight/2+i)*SrcWidth+(x-KWidth/2+j);
					//temp += (pSrcImage[border]*gmat[i*KWidth+j]);
					temp += (pSrcImage[border]*pKernel[i*KWidth+j]);
				}
			}
			pDstImage[index] = temp;
	}
	else
	{
		pDstImage[index] =0;
	} 
}

void gpu_Filter(float *pcuSrc, float *pcuDst, int w, int h, float *cuGkernel, int kernel_size)
{ 
	// number of threads
	int threadx = 20;
	int thready = 10;
	dim3 block = dim3(threadx,thready,1);
	dim3 grid = dim3(w/block.x,h/block.y);
 
 	cuda_Filter2D<<< grid, block, sizeof(float)*kernel_size*kernel_size >>>(pcuSrc,w,h,cuGkernel,kernel_size,kernel_size,pcuDst);
	//cuda_Filter2D<<< grid, block>>>(pcuSrc,w,h,cuGkernel,kernel_size,kernel_size,pcuDst);
	cudaThreadSynchronize(); 

	/*float *PrintKernel = new float[kernel_size*kernel_size];
	cudaMemcpy(PrintKernel, cuGkernel, kernel_size*kernel_size*sizeof(float), cudaMemcpyDeviceToHost);

	for (int i = 0; i<kernel_size; i++){
		for (int j =0; j<kernel_size; j++)
		{
			printf("%f\t",PrintKernel[i*kernel_size+j]);
		}
		printf("\n");
	}*/
}