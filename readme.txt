convolution (Garbor & Gaussian filter)

1. Library Requirement
CUDA 6.0 (or higher version)
OpenCV 2.4.10

2. Run
1) You can modify input file name in main function
IplImage* pInput = cvLoadImage("hd.jpg",CV_LOAD_IMAGE_GRAYSCALE); //load image using opencv

2) You can choose convolution type
// Gabor filter for texture analysis
Seq_Gaborfilter(1.5,(90.0*3.141593/180),(0.55),(90.0*3.141593/180),kernel_size,Gkernel);
// Gaussian filter for smoothing
Seq_Gaussianfilter(3.0,kernel_size,Gkernel);

! You should remove one of them by comment or somethine


3) Compile the program




